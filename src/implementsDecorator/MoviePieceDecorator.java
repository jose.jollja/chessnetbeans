/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementsDecorator;

import chess.ChessGameBoard;
import chess.ChessGamePiece;
import chess.CoordenadaPiece;
import java.util.ArrayList;

/**
 *
 * @author RAMIREZ
 */
public abstract class MoviePieceDecorator implements MoviePiece {

    private MoviePiece moviepiece;
    static final int UNASSIGNED = -1;
    static final int            BLACK      = 0;
    static final int            WHITE      = 1;
    
    public boolean isPieceOnScreen(int row, int col) {
        return row >= 0 && row <= 7 && col >= 0 && col <= 7;
    }

    public boolean isEnemy(ChessGameBoard board, int row, int col) {
        if (row > 7 || col > 7 || row < 0 || col < 0) {
            return false;
        }
        ChessGamePiece enemyPiece
                = board.getCell(row, col).getPieceOnSquare() == null
                ? null
                : board.getCell(row, col).getPieceOnSquare();
        if (enemyPiece == null
                || getColorPiece() == MoviePieceDecorator.UNASSIGNED
                || enemyPiece.getColorOfPiece() == MoviePieceDecorator.UNASSIGNED) {
            return false;
        }
        if (this.getColorPiece() == MoviePieceDecorator.WHITE) {
            if (enemyPiece.getColorOfPiece() == MoviePieceDecorator.BLACK) {
                return true;
            } else {
                return false;
            }
        } else {
            if (enemyPiece.getColorOfPiece() == MoviePieceDecorator.WHITE) {
                return true;
            } else {
                return false;
            }
        }
    }

    public MoviePieceDecorator(MoviePiece moviePiece) {
        this.moviepiece = moviePiece;
    }

    public MoviePiece getMoviePiece() {
        return this.moviepiece;
    }

    public void setMoviePieve(MoviePiece moviePiece) {
        this.moviepiece = moviePiece;
    }
    public ArrayList<String> getMoviePosible(){
        return getMoviePiece().getMoviePosible();
    }

    @Override
    public int getColorPiece() {
        return getMoviePiece().getColorPiece();
    }

    @Override
    public String getType() {
        return getMoviePiece().getType();
    }

    @Override
    public CoordenadaPiece getCoordenada() {
        return getMoviePiece().getCoordenada();
    }

}
