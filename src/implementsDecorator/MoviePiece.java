/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementsDecorator;

import chess.CoordenadaPiece;
import java.util.ArrayList;

/**
 *
 * @author RAMIREZ
 */
public interface MoviePiece {
    
    public int getColorPiece();
    public String getType();
    public CoordenadaPiece getCoordenada();
    public ArrayList<String> getMoviePosible();
}
