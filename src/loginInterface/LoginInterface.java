/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loginInterface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import implementNullObjectPattern.UserFactory;
import implementNullObjectPattern.Autentificacion;

/**
 *
 * @author User
 */
public class LoginInterface {
    boolean state = false;
    
    String username;
    String password;
    
    JFrame frame = new JFrame("Login");
    JPanel panel =new JPanel();
    JLabel EtiquetaLogin = new JLabel("LOGIN");
    JLabel EtiquetaUsername= new JLabel("Username: ");
    JLabel EtiquetaPassword = new JLabel("Password: ");
    JLabel EtiquetaResultado = new JLabel("");
    
    
    JTextField TxTUsername = new JTextField("");
    JTextField TxTPassword = new JTextField("");
    
    
    JButton button = new JButton("ENTRAR");
    
    public LoginInterface(){
    
        button.addActionListener((ActionEvent e) -> {
            username = TxTUsername.getText();
            password = TxTPassword.getText();
            
            UserFactory user_create = new UserFactory(username,password);
            Autentificacion user = user_create.getUser();
            
            if(user.validateUsername() && user.validatePassword()){
                
                state = true;
                frame.dispose();
                
            }
            else{
                state = true;
                frame.dispose();
                
               // EtiquetaResultado.setText("Usario no valido");
            }
        });
        
        
        
        this.EtiquetaLogin.setBounds(180, 0, 270, 25);
        this.EtiquetaUsername.setBounds(10,25, 270, 25);
        this.TxTUsername.setBounds(150, 25, 200, 25);
        this.EtiquetaPassword.setBounds(10, 50, 270, 25);
        this.TxTPassword.setBounds(150, 50, 200, 25);
        
        this.button.setBounds(120, 80, 150, 30);
        
        this.EtiquetaResultado.setBounds(120, 110, 150, 30);
        
        this.frame.add(EtiquetaLogin);
        this.frame.add(EtiquetaUsername);
        this.frame.add(TxTUsername);
        this.frame.add(EtiquetaPassword);
        this.frame.add(TxTPassword);
        this.frame.add(button);
        this.frame.add(EtiquetaResultado);
        
        frame.setSize(380,200);
        frame.setLocation(600, 400);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.setVisible(true);
        
        
        
        
    }
    
    public boolean getState(){
        return state;
    }

    public boolean getVisible(){
        return frame.isVisible();
    }
            
}
