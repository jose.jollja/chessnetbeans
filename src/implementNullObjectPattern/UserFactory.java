/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementNullObjectPattern;

/**
 *
 * @author User
 */
public class UserFactory {
    private String username;
    private String password;
    
    public UserFactory(String username, String password){
        this.username = username;
        this.password = password;
    }
    
    public Autentificacion getUser(){
    
        if ( !this.username.equals("") && !this.password.equals("")){
            return new User(this.username, this.password);
        }
        return new NullUser();
    }
}
