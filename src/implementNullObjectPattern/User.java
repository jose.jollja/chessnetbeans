/*
 
    En esta sección usamos el patrón strategy para optimizar nuestras pruebas.
 
 */
package implementNullObjectPattern;

import implementStrategyPattern.Contexto;

//Podriamos hacer la prueba usando una base de datos o un usario test
import implementStrategyPattern.AuthDatabase;
import implementStrategyPattern.authTestUser;


/**
 *
 * @author User
 */
public class User implements Autentificacion {
    private String username;
    private String password;
    private Contexto user;
    
    public User(String username, String password){
        this.username = username;
        this.password = password;
        
        this.user = new Contexto(new authTestUser());
        
        //Si deseamos usar los usarios de la base de datos
        //this.user = new Contexto(new AuthDatabase());
    }
    
    @Override
    public boolean validateUsername(){
        //El username puede ser traido de una base de datos, en este caso usaremos uno de prueba
        String userValidate = this.user.buscarUsario();
        
        if (this.username.equals(userValidate)){
            
            return true;
            
        }
        return false;
    }
    
    @Override
    public boolean validatePassword(){
        //El password puede ser traido de una base de datos, en este caso usaremos uno de prueba
        String passwordValidate = this.user.buscarPassword();
        
        if (this.password.equals(passwordValidate)){
            
            return true;
        }
        return false;
    }
    
}
