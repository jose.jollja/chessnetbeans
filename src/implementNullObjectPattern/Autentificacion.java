/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementNullObjectPattern;

/**
 *
 * @author User
 */
public interface Autentificacion {
    
    boolean validateUsername();
    boolean validatePassword();
    
}
