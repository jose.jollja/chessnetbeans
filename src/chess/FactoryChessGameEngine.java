/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess;

/**
 *
 * @author Mary
 */
public class FactoryChessGameEngine {

    public static IChessGameEngine getIChessGameEngine(String tipo) {
        if (tipo.equals("tipo1")) {
            return new MotorDeJuegoTipo1();
        } else {
            return new ChessGameEngine();
        }

    }
}
