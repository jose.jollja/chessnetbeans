/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess;

import java.util.ArrayList;

/**
 *
 * @author RAMIREZ
 */
/**public abstract class Moves {
    
     protected ArrayList<String> calculateSouthMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = pieceRow + 1; i < 8 && count < numMoves; i++ ){
                if ( ( board.getCell( i, pieceColumn ).getPieceOnSquare()
                    == null || isEnemy( board, i, pieceColumn ) ) ){
                    moves.add( i + "," + pieceColumn );
                    count++;
                    if ( isEnemy( board, i, pieceColumn ) ){
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
     
     protected ArrayList<String> calculateNorthMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = pieceRow - 1; i >= 0 && count < numMoves; i-- ){
                if ( ( board.getCell( i, pieceColumn ).getPieceOnSquare()
                    == null || isEnemy( board, i, pieceColumn ) ) ){
                    moves.add( i + "," + pieceColumn );
                    count++;
                    if ( isEnemy( board, i, pieceColumn ) ){
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
     
    protected ArrayList<String> calculateEastMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = pieceColumn + 1; i < 8 && count < numMoves; i++ ){
                if ( ( board.getCell( pieceRow, i ).getPieceOnSquare()
                    == null || isEnemy( board, pieceRow, i ) ) ){
                    moves.add( pieceRow + "," + i );
                    count++;
                    if ( isEnemy( board, pieceRow, i ) ){
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
    
    protected ArrayList<String> calculateWestMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = pieceColumn - 1; i >= 0 && count < numMoves; i-- ){
                if ( ( board.getCell(pieceRow, i ).getPieceOnSquare()
                    == null || isEnemy( board, pieceRow, i ) ) ){
                    moves.add( pieceRow + "," + i );
                    count++;
                    if ( isEnemy( board, pieceRow, i ) ){
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
    
    protected ArrayList<String> calculateNorthWestMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = 1; i < 8 && count < numMoves; i++ ){
                if ( isOnScreen( pieceRow - i, pieceColumn - i )
                    && ( board.getCell( pieceRow - i,
                        pieceColumn - i ).getPieceOnSquare() == null ) ){
                    moves.add( ( pieceRow - i ) + "," + ( pieceColumn - i ) );
                    count++;
                }
                else if ( isEnemy( board, pieceRow - i, pieceColumn - i ) ){
                    moves.add( ( pieceRow - i ) + "," + ( pieceColumn - i ) );
                    count++;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
    
    protected ArrayList<String> calculateNorthEastMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = 1; i < 8 && count < numMoves; i++ ){
                if ( isOnScreen( pieceRow - i, pieceColumn + i )
                    && ( board.getCell( pieceRow - i,
                        pieceColumn + i).getPieceOnSquare() == null ) ){
                    moves.add( ( pieceRow - i ) + "," + ( pieceColumn + i ) );
                    count++;
                }
                else if ( isEnemy( board, pieceRow - i, pieceColumn + i ) ){
                    moves.add( ( pieceRow - i ) + "," + ( pieceColumn + i ) );
                    count++;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
    protected ArrayList<String> calculateSouthWestMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = 1; i < 8 && count < numMoves; i++ ){
                if ( isOnScreen( pieceRow + i, pieceColumn - i )
                    && ( board.getCell( pieceRow + i,
                        pieceColumn - i ).getPieceOnSquare() == null ) ){
                    moves.add( ( pieceRow + i ) + "," + ( pieceColumn - i ) );
                    count++;
                }
                else if ( isEnemy( board, pieceRow + i, pieceColumn - i ) ){
                    moves.add( ( pieceRow + i ) + "," + ( pieceColumn - i ) );
                    count++;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
    
    protected ArrayList<String> calculateSouthEastMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        if ( isPieceOnScreen() ){
            for ( int i = 1; i < 8 && count < numMoves; i++ ){
                if ( isOnScreen( pieceRow + i, pieceColumn + i )
                    && ( board.getCell( pieceRow + i,
                        pieceColumn + i ).getPieceOnSquare() == null ) ){
                    moves.add( ( pieceRow + i ) + "," + ( pieceColumn + i ) );
                    count++;
                }
                else if ( isEnemy( board, pieceRow + i, pieceColumn + i ) ){
                    moves.add( ( pieceRow + i ) + "," + ( pieceColumn + i ) );
                    count++;
                    break;
                }
                else
                {
                    break;
                }
            }
        }
        return moves;
    }
   
    
    
} **/
