/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess.FactoryMethodRecursos;

/**
 *
 * @author Usuario
 */
public class RecursoMensaje implements IRecursos{

    public RecursoMensaje() {
    }
    
     @Override
    public String enviar(int a , int b) {
        
        if(a==1){
            switch(b){
                case 1: {return ("Un ajedrez nuevo  "
                + "se ha iniciado el juego. El jugador 1 (blanco) jugará  "
                + "contra el jugador 2 (negro). ¡COMENZAR! ");} // Reset 
                case 2: {return ("Un nuevo ajedrez "
                + "el juego ha comenzado. El jugador 1 (blanco) jugará "
                + "contra el jugador 2 (negro). ¡COMIENZA!");} // Reset
                case 3: {return ("¿Quieres jugar de nuevo? ");} // AskUsertiPlayAgain
                case 4: {return ("Se acabó el juego - STALEMATE. Ambos deberían ir llora en un rincón! ");} // CheckGameConditions STALEMATE
                case 5: {return ("Juego terminado - CHECKMATE.  pierde y debería irse llora en un rincón! ");} // CheckGameConditions CHECKMATE
                case 6: {return ("tu rey está bajo control! Tu próximo movimiento debe ser él fuera de control o estás jodido. ");} // CheckGameConditions checkKing
                case 7: {return ("¡Intentaste recoger la pieza del otro jugador! Consigue unos vasos y elige una casilla válida");} // ActioDeterminate píece incorrect
                case 8: {return ("¡Intentaste recoger un cuadrado vacío! Consigue unos vasos y elige una casilla válida");} // ActioDeterminate squareless
                case 9: {return ("no es válido o no es legal para esta pieza. Elige otra ubicación de movimiento ¡e intenta usar tu cerebro esta vez!");} // ActioDeterminate Illegal
              
                default: { return "Unknown Message"; }
            }

        }else {
            return "Unknown Message";
            
        }
    }

}
