/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess.FactoryMethodRecursos;

/**
 *
 * @author Usuario
 */
public class RecursoImagen implements IRecursos {
    
    private static RecursoImagen sRecursoImagen;

    public RecursoImagen() {
    }

    
    public static RecursoImagen getRecursoImagen(){
        if(sRecursoImagen == null){
            sRecursoImagen = new RecursoImagen();
        }
        return sRecursoImagen;
    }
    
    @Override
    public String enviar(int a,int b) {
        
        if(a==1){
            switch(b){
                case 1: {return "chessImages/WhiteKing.gif";} // King 1 
                case 2: {return "chessImages/WhiteQueen.gif";} // Queen 2
                case 3: {return "chessImages/WhiteBishop.gif";} // bishop 3
                case 4: {return "chessImages/WhitePawn.gif";} // pawn 4
                case 5: {return "chessImages/WhiteRook.gif";} // rook 5
                case 6: {return "chessImages/WhiteKnight.gif";} // Knight 6
                default: { return "chessImages/default-Unassigned.gif"; }
            }

        }else {
            switch(b){
                case 1: {return "chessImages/BlackKing.gif";} // King 1
                case 2: {return "chessImages/BlackQueen.gif";} // Queen 2
                case 3: {return "chessImages/BlackBishop.gif";} // bishop 3
                case 4: {return "chessImages/BlackPawn.gif";} // pawn 4
                case 5: {return "chessImages/BlackRook.gif";} // rook 5
                case 6: {return "chessImages/BlackKnight.gif";} // Knight 6
                default: { return "chessImages/default-Unassigned.gif"; }
            }
        }
    }
 
}
