
package chess.FactoryMethodRecursos;

/**
 *
 * @author Usuario
 */
public class RecursosFabrica {
    
    private static RecursosFabrica sgRecursosFabrica;
    
    
    public static RecursosFabrica getRecursosFabrica(){
        if(sgRecursosFabrica == null){
            sgRecursosFabrica = new RecursosFabrica();
        }
        return sgRecursosFabrica;
    }
    
    public IRecursos getRecurso(String sourc){
        if(sourc == null){
        return new RecursoVacio();
        }else if(sourc.equalsIgnoreCase("Images")){
        return RecursoImagen.getRecursoImagen();
        }else if(sourc.equalsIgnoreCase("Mensajes")){
        return new RecursoMensaje();   
        }
        return new RecursoVacio();

    }
    
    
    
    
    
}
