
package chess;

import javax.swing.*;
import loginInterface.LoginInterface;
// -------------------------------------------------------------------------
/**
 * Shows the GUI for the Chess game.
 * 
 * @author Ben Katz (bakatz)
 * @author Myles David II (davidmm2)
 * @author Danielle Bushrow (dbushrow)
 * @version 2010.11.17
 */
public class ChessMain{
     int adata=2222;
    // ----------------------------------------------------------
    /**
     * Creates the GUI for Chess.
     * 
     * @param args
     *            command line arguments, not used
     */
    public static void main( String[] args ){
         int adato=1111;
        
        //llamamos al login
        LoginInterface login = new LoginInterface();
        
        /*
        El usario si se usa testUsers es: test
                                password: 1234
        
        El usario si se usa Database es: jose
                                password: 1234
        
        Actualemente se encuentra en el estado testUsers en el patron strategy
        */
        
        
        boolean state = true;
        while(state){
            try{
                Thread.sleep(90);
            }
            catch(InterruptedException ex)
            {
                Thread.currentThread().interrupt();
            }
            if(login.getState()){
                state = false;
            }else{
                if(!login.getVisible()){
                System.exit(0);
            }
            }
            
        }
                           
        IChessGameEngine chessGameEngine = new ChessGameEngine();       
        IChessGameEngine chessGameEnginePersonalized = new ChessGameEnginePersonalized();       

        JFrame frame = new JFrame( "YetAnotherChessGame 1.0" );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.getContentPane().add( new ChessPanel(chessGameEngine,3));
        frame.pack();
        frame.setVisible( true );
        
       
        
        
    }

}
