/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess.MoviesAll;

import chess.ChessGameBoard;
import chess.CoordenadaPiece;
import implementsDecorator.MoviePiece;
import implementsDecorator.MoviePieceDecorator;
import java.util.ArrayList;

/**
 *
 * @author RAMIREZ
 */
public class MoveNorte extends MoviePieceDecorator {

    public MoveNorte(MoviePiece moviePiece) {
        super(moviePiece);
    }
    
    public ArrayList<String> calculateMoves(
        ChessGameBoard board,
        int numMoves ){
        ArrayList<String> moves = new ArrayList<String>();
        int count = 0;
        CoordenadaPiece coord = getMoviePiece().getCoordenada();
        if ( isPieceOnScreen(coord.getRow(), coord.getCol()) ){
            for ( int i = coord.getRow() - 1; i >= 0 && count < numMoves; i-- ){
                if ( ( board.getCell( i, coord.getCol() ).getPieceOnSquare()
                    == null || isEnemy( board, i, coord.getCol() ) ) ){
                    moves.add( i + "," + coord.getCol() );
                    count++;
                    if ( isEnemy( board, i, coord.getCol() ) ){
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        ArrayList<String> movimientos = getMoviePiece().getMoviePosible();
        movimientos.addAll(moves);
        return movimientos;
    }
}
