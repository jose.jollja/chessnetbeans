
package chess;


public class ImagePieces {

    private static ImagePieces mImagepieces;
    
    public ImagePieces() {
    }
    
    
     public static ImagePieces getImagePieces(){
        if(mImagepieces == null){
            mImagepieces = new ImagePieces();
        }
        return mImagepieces;
    }

     public String recursos(int a,char pieza,int tipo){
        if(a==1){
            switch(pieza){
                case 'k': {return "chessImages/WhiteKing.gif";}
                case 'q': {return "chessImages/WhiteQueen.gif";} // Queen
                case 'b': {return "chessImages/WhiteBishop.gif";} // bishop
                case 'p': {return "chessImages/WhitePawn.gif";} // pawn
                case 'r': {return "chessImages/WhiteRook.gif";} // rook
                case 'h': {return "chessImages/WhiteKnight.gif";} // Knight
                default: { return "chessImages/default-Unassigned.gif"; }
            }

        }else {
            switch(pieza){
                case 'k': {return "chessImages/BlackKing.gif";} // King
                case 'q': {return "chessImages/BlackQueen.gif";} // Queen
                case 'b': {return "chessImages/BlackBishop.gif";} // bishop
                case 'p': {return "chessImages/BlackPawn.gif";} // pawn
                case 'r': {return "chessImages/BlackRook.gif";} // rook
                case 'h': {return "chessImages/BlackKnight.gif";} // Knight
                default: { return "chessImages/default-Unassigned.gif"; }
            }
        }     
    }
}



