

package chess;
import java.util.ArrayList;

public interface ChessCalculateEastMovePiece {
    ArrayList<String> calculateEastMoves(ChessGameBoard board, int numMoves);
}
