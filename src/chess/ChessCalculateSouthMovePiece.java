
package chess;
import java.util.ArrayList;
public interface ChessCalculateSouthMovePiece {
    ArrayList<String> calculateSouthMoves(ChessGameBoard board,int numMoves );
    ArrayList<String> calculateSouthWestMoves(ChessGameBoard board,int numMoves );
    ArrayList<String> calculateSouthEastMoves(ChessGameBoard board,int numMoves );

}
