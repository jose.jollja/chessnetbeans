
package chess;
import java.util.ArrayList;

public interface ChessCalculateNorthMovePiece {
     
    ArrayList<String> calculateNorthMoves(ChessGameBoard board, int numMoves);
    ArrayList<String> calculateNorthWestMoves(ChessGameBoard board,int numMoves );
    ArrayList<String> calculateNorthEastMoves(ChessGameBoard board,int numMoves );

}
