
package chess;
import java.util.ArrayList;
public interface ChessCalculateWestMovePiece {
    ArrayList<String> calculateWestMoves(ChessGameBoard board,int numMoves );

}
