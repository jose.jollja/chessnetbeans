/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chess;

import java.awt.event.MouseEvent;

/**
 *
 * @author Mary
 */
public interface IChessGameEngine {
    
    public void reset();
    public void nextTurn();
    public int getCurrentPlayer();
    public boolean playerHasLegalMoves( int playerNum );
    public boolean selectedPieceIsValid();
    public boolean isKingInCheck( boolean checkCurrent );
    public void askUserToPlayAgain( String endGameStr );
    public void checkGameConditions();
    public int determineGameLost();
    public void determineActionFromSquareClick( MouseEvent e );
    public void setChessGameBoard(ChessGameBoard chessGameBoard);

}
