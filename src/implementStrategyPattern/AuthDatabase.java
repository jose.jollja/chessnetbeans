/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementStrategyPattern;

/**
 *
 * @author User
 */
public class AuthDatabase implements StrategyTestUsers{
    
    public String buscarUsuario(){
        //Se tendría que hacer la petición a una base de datos para obtener si el usuairo existe
        String user = "jose";
        return user;
    };
    
    public String buscarPassword(){
        //Se tendría que hacer la petición a una base de datos para obtener si la contraseña existe
        String password = "1234";
        return password;
    }
}
