/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementStrategyPattern;

/**
 *
 * @author User
 */
public class Contexto {
    
    private StrategyTestUsers strategy;
    
    public Contexto(StrategyTestUsers strategy){
        this.strategy = strategy;
    }
    
    public String buscarUsario(){
        return this.strategy.buscarUsuario();
    }
    
    public String buscarPassword(){
        return this.strategy.buscarPassword();
    }
}
